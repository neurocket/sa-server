import cors from 'cors';
import express from 'express';

import {
  getModelByGMDH,
  useGMDHModel,
} from './sa';

const app = express();
const corsOptions = {
  methods: 'POST',
};

app.all('/*', cors(corsOptions));

app.use('/*', express.json({limit: '100mb'}));

app.post('/teaching', (req, res) => {
  let payload = req.body;
  let model = getModelByGMDH(payload);
  res.send(model);
});

app.post('/using', (req, res) => {
  let payload = req.body;
  let prediction = useGMDHModel(payload);
  res.send(prediction);
});

app.listen(8090, () => console.log('Server is listening on port 8090!'));
