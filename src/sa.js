import combinatorics from 'js-combinatorics';
import math from 'mathjs';
import {performance} from 'perf_hooks';

let mathNode = math.expression.node;

function getFilledArray(length, content) {
  return new Array(length).fill(content);
}

class Polinom {
  static toXAccessor(node) {
    switch (node.type) {
      case 'OperatorNode': {
        return node.map(Polinom.toXAccessor);
      }
      case 'SymbolNode': {
        let index = parseInt(node.name.slice(1));
        return new mathNode.AccessorNode(
            new mathNode.SymbolNode('X'),
            new mathNode.IndexNode([new mathNode.ConstantNode(index)]));
      }
      default: {
        return node;
      }
    }
  }

  static getEvalWithXAccessor(node) {
    let f = Polinom.toXAccessor(node).compile().eval;
    return X => f({X});
  };

  constructor(polinomExpression) {
    polinomExpression = polinomExpression.replace(/a(\d+)/gui, 'a$1');
    polinomExpression = polinomExpression.replace(/[xf](\d+)/gui, 'x$1');
    let aSymbols = polinomExpression.
        match(/(a\d+)(?!.*\1)/gu).
        sort((symbol1, symbol2) =>
            parseInt(symbol1.slice(1)) - parseInt(symbol2.slice(1)));
    let aObj = aSymbols.reduce((obj, sym) => {
      obj[sym] = 0;
      return obj;
    }, {});
    this._evaluators = aSymbols.
        map(symbol => {
          aObj[symbol] = 1;
          let aNode = math.simplify(polinomExpression, aObj);
          aObj[symbol] = 0;
          return {
            node: aNode,
            f: Polinom.getEvalWithXAccessor(aNode),
          };
        });
  }

  get evaluators() {
    return this._evaluators;
  }
}

class FilterModel {
  static expandSize(size, cell) {
    return cell.map((value, i) => Math.max(value + 1, size[i]));
  }

  constructor(polinom, xCells, yCell, data) {
    let size = getFilledArray(2, 0);
    size = xCells.reduce(FilterModel.expandSize, size);
    size = FilterModel.expandSize(size, yCell);
    this._xCells = xCells;
    this._yCell = yCell;

    let A = [], B = [];
    for (let i = 0; i <= data.length - size[0]; i++) {
      let X = xCells.map(cell => data[cell[0] + i][cell[1]]);
      A[i] = polinom.evaluators.map(evaluator => {
        return evaluator.f(X);
      });
      B[i] = data[yCell[0] + i][yCell[1]];
    }

    let normLength = A[0].length;
    let normA = [], normB = [];
    for (let normIndex = 0; normIndex < normLength; normIndex++) {
      let ASum = getFilledArray(normLength, 0);
      let BSum = 0;
      for (let i = 0; i < A.length; i++) {
        let multiplier = A[i][normIndex];
        for (let j = 0; j < normLength; j++) {
          ASum[j] += A[i][j] * multiplier;
        }
        BSum += B[i] * multiplier;
      }
      normA[normIndex] = ASum;
      normB[normIndex] = BSum;
    }

    let aValues = math.transpose(math.lusolve(normA, normB))[0];

    let trainedModelNode = polinom.evaluators.
        reduce((trainedModelNode, evaluator, index) => {
          let trainedANode = new mathNode.OperatorNode('*', 'multiply', [
            new mathNode.ConstantNode(aValues[index]),
            evaluator.node,
          ]);
          if (!trainedModelNode) {
            return trainedANode;
          }
          return new mathNode.OperatorNode('+', 'add', [
            trainedModelNode,
            trainedANode,
          ]);
        }, null);

    this._evaluator = {
      node: trainedModelNode,
      f: Polinom.getEvalWithXAccessor(trainedModelNode),
    };
  }

  get evaluator() {
    return Object.assign({}, this._evaluator);
  }

  predict(offset, size, data) {
    let predictions = [];
    let offsetForY = offset[0] - this._yCell[0];
    for (let i = offsetForY, iEnd = i + size[0]; i < iEnd; i++) {
      let X = this._xCells.map(cell => {
        //TODO: use last predictions if it`s needed
        return data[cell[0] + i][cell[1]];
      });
      predictions.push(this._evaluator.f(X));
    }
    return predictions;
  }
}

export function getModelByGMDH(
    {polinom: polinomStr, data, selectionCount, selectionLength}) {
  let polinom = new Polinom(polinomStr);
  let learningData = data.slice(0, data.length / 2);
  let checkingData = data.slice(learningData.length);

  function getCombinations(start, end, size) {
    let row = [];
    for (let i = start; i <= end; i++) {
      row.push(i);
    }
    return combinatorics.bigCombination(row, size);
  }

  let bestSelectionRow = [];
  let previousSelectionRow = [];
  for (let selectionCounter = 0;
       selectionCounter < selectionCount; selectionCounter++) {
    try {
      let selectionRow
          = getCombinations(0, learningData[0].length - 2, 2).
          map(combination => {
            let xCells = combination.reduce(
                (xCells, dataIndex, xCellsIndex) => {
                  xCells[xCellsIndex] = [0, dataIndex + 1];
                  return xCells;
                }, []);
            let yCell = [0, 0];
            let s = {combination};
            s.model = new FilterModel(polinom, xCells, yCell, learningData);
            s.checkingPrediction = s.model.
                predict([0], [checkingData.length], checkingData);
            s.quadDeviation = ((s.checkingPrediction.reduce(
                (sum, item, index) =>
                    sum + (checkingData[index][0] - item) ** 2, 0)
                / (s.checkingPrediction.length)));
            return s;
          }).
          sort((s1, s2) => s1.quadDeviation - s2.quadDeviation).
          slice(0, selectionLength).
          map(s => {
            s.learningPrediction = s.
                model.predict([0], [learningData.length], learningData);
            return s;
          });

      if (selectionCounter > 0) {
        selectionRow = selectionRow.map(s => {
          s.branch = {
            node: s.model.evaluator.node.toString(),
            branches: s.combination.
                map(cIndex => previousSelectionRow[cIndex].branch),
          };
          return s;
        });
      } else {
        selectionRow = selectionRow.map(s => {
          s.branch = {
            node: s.model.evaluator.node.toString(),
            leafs: s.combination,
          };
          return s;
        });
      }

      learningData = learningData.map((learningDataRow, indexI) => {
        learningDataRow = [learningDataRow[0]];
        selectionRow.forEach((s, indexJ) =>
            learningDataRow[indexJ + 1] = s.learningPrediction[indexI]);
        return learningDataRow;
      });
      checkingData = checkingData.map((checkingDataRow, indexI) => {
        checkingDataRow = [checkingDataRow[0]];
        selectionRow.forEach((s, indexJ) =>
            checkingDataRow[indexJ + 1] = s.checkingPrediction[indexI]);
        return checkingDataRow;
      });

      if (selectionCounter === 0 ||
          bestSelectionRow[0].quadDeviation > selectionRow[0].quadDeviation) {
        bestSelectionRow = selectionRow;
      } else if (selectionRow[0].quadDeviation >
          bestSelectionRow[0].quadDeviation * 2) {
        break;
      }

      previousSelectionRow = selectionRow;

      console.log(`SR=${selectionCounter}`,
          `Best QD=${bestSelectionRow[0].quadDeviation}`,
          `Current QD=${selectionRow[0].quadDeviation}`);
    } catch (e) {
      console.dir(e);
      break;
    }
  }

  let model = bestSelectionRow[0].branch;
  model.quadDeviation = bestSelectionRow[0].quadDeviation;
  return model;
}

function useModel(model, data) {
  if (model.branches) {
    let branchesPredictions = model.branches.
        map(branch => useModel(branch, data));
    data = data.map((dataRow, indexI) => {
      dataRow = [];
      branchesPredictions.forEach((prediction, indexJ) =>
          dataRow[indexJ] = prediction[indexI]);
      return dataRow;
    });
    let predictions = [];
    let f = Polinom.getEvalWithXAccessor(math.parse(model.node));
    for (let i = 0; i < data.length; i++) {
      let X = data[i];
      predictions.push(f(X));
    }
    return predictions;
  } else {
    let predictions = [];
    let f = Polinom.getEvalWithXAccessor(math.parse(model.node));
    for (let i = 0; i < data.length; i++) {
      let X = model.leafs.map(cell => {
        return data[i][cell];
      });
      predictions.push(f(X));
    }
    return predictions;
  }
}

export function useGMDHModel({model, data}) {
  return useModel(model, data).map(item => [item]);
}
